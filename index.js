/* Libraries */

const fs = require("fs");
const path = require("path");

const Parser = require("jison").Parser;
const Discord = require("discord.js");
const bot = new Discord.Client();
const {Semaphore} = require("await-semaphore");
const axios = require("axios").default

require("dotenv").config();

const search = require("./search");

/* Constants */

const TOKEN = process.env.TOKEN;
const ADMIN_PASSWORD = process.env.ADMIN_PASSWORD;

const IMAGES_FOLDER = "images"
const GRAMMAR_FILE = "grammar.jison";
const ALLOWED_IMAGES = [".png", ".jpg", ".jpeg"];

/* Mutex */

const mutex = new Semaphore(1);
let currentMSG;
let release = () => undefined;

/* Admin */

let admins = [];

/* Check folders */

if (!fs.existsSync(path.join(".", IMAGES_FOLDER)))
	fs.mkdirSync(path.join(".", IMAGES_FOLDER));

/* Parser */

const parser = new Parser(fs.readFileSync(path.join(".", GRAMMAR_FILE), "utf-8"));

parser.yy = {
	processKeywords: async keywords => {
		const bestId = await search.searchImage(keywords);
		const filename = fs.readdirSync(path.join(".", IMAGES_FOLDER)).find(el => new RegExp(bestId + "[.].*").test(el));
		if (filename)
			currentMSG.channel.send({files: [path.join(".", IMAGES_FOLDER, filename)]});
		else
			currentMSG.channel.send("Désolé, je n'ai rien trouvé...");
		release();
		return;
	},
	addImage: keywords => {
		if (admins.includes(currentMSG.author.id)) {
			currentMSG.attachments.forEach(attach => {
				if (ALLOWED_IMAGES.includes(path.extname(attach.url))) {
					axios({
						method: "get",
						url: attach.url,
						responseType: "stream"
					})
					.then(async res => {
						const newFilename = await search.insertImage(keywords, path.extname(attach.url));
						const newImagePath = path.join(".", IMAGES_FOLDER, newFilename);
						res.data.pipe(fs.createWriteStream(newImagePath));
						currentMSG.channel.send("Image sauvegardée !");
						return;
					})
					.catch(console.error);
				}
			});
		} else {
			currentMSG.channel.send("Wow wow wow ! Tu n'es pas admin !?");
		}
		release();
	},
	loginAdmin: password => {
		if (password == ADMIN_PASSWORD) {
			if (!admins.includes(currentMSG.author.id)) {
				admins.push(currentMSG.author.id);
				currentMSG.channel.send("Connecté");
			} else {
				currentMSG.channel.send("T'es déjà connecté !");
			}
		}
		release();
	}
};

/* Bot */

bot.on("ready", function() {
	console.info(`Logged in as ${bot.user.tag}!`);
});

bot.on("message", async function(msg) {
	try {
		release = await mutex.acquire();
		currentMSG = msg;
		parser.parse(msg.content);
	} catch {
		release();
	}
});

bot.login(TOKEN);
