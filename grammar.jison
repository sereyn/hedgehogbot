%{


%}

%lex

%%
\s+ /* skip whitespace */
[!]hbot return 'HBOT';
search return 'SEARCH';
add return 'ADD';
admin return 'ADMIN';
[a-zA-Z0-9]+ return 'KEYWORD';

/lex

%start command

%%

command:
	HBOT SEARCH keywords {
		yy.processKeywords($3);
	}
	| HBOT ADD keywords {
		yy.addImage($3);
	}
	| HBOT ADMIN KEYWORD {
		yy.loginAdmin($3);
	}
	;

keywords:
	keywords KEYWORD {
		$$ = $1.concat($2);
	}
	| KEYWORD {
		$$ = [$1];
	}
	;
