/* Libraries */

const fs = require("fs");
const path = require("path");

const stringSimilarity = require("string-similarity");
const sqlite3 = require("sqlite3").verbose();

/* Constants */

const DB_FILE = "data.db";
const ID_ALPHABET = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_";
const ID_LEN = 12;

/* Create DB */

if (!fs.existsSync(path.join(".", DB_FILE)))
	fs.closeSync(fs.openSync(path.join(".", DB_FILE), "w"));

const db = new sqlite3.Database(path.join(".", DB_FILE));

db.run(`
CREATE TABLE IF NOT EXISTS keywords(
	id VARCHAR(12) NOT NULL,
	keyword VARCHAR(24) NOT NULL,
	PRIMARY KEY(id)
)
`);

/* Functions */

const getKeywords = function() {
	return new Promise((resolve, reject) => {
		db.all(`SELECT * FROM keywords`, (err, rows) => {
			if (err)
				reject(err);
			resolve(rows);
		});
	});
}

const idExists = async function(id) {
	db.all(`SELECT count(*) AS number FROM keywords WHERE id = '${id}'`, function(err, rows) {
		if (err) {
			console.error(err);
			return undefined;
		} else {
			return rows[0].number > 0;
		}
	});
};

const generateId = function() {
	id = "";
	for (let i = 0; i < ID_LEN; i++)
		id += ID_ALPHABET[Math.floor(Math.random() * ID_ALPHABET.length)];
	return id;
};

const generateUniqueId = async function() {
	let id, res;
	do {
		id = generateId();
		res = await idExists(id) === true;
		if (res == undefined)
			throw new Error("Database request failed");
	} while (res);
	return id;
};

/* Exported functions */

module.exports = {
	insertImage: async (keywords, ext) => {
		const newId = await generateUniqueId();
		keywords.forEach(keyword => {
			db.run(`INSERT INTO keywords (id, keyword) VALUES ('${newId}', '${keyword}')`, (_, err) => {
				if (err)
					console.error(err);
			});
		});
		return newId + ext;
	},
	searchImage: async keywords => {
		const rows = await getKeywords();
		const scores = {};

		rows.forEach(row => {
			if (!(row.id in scores))
				scores[row.id] = 0;
			keywords.forEach((keyword, keywordIndex) => {
				const keywordWeight = keywords.length - keywordIndex;
				const similarity = stringSimilarity.compareTwoStrings(keyword, row.keyword);
				scores[row.id] += Math.pow(similarity, keywordWeight);
			});
		});

		let max = -1;
		let maxId = undefined;
		Object.keys(scores).forEach(id => {
			if (max == -1 || max < scores[id]) {
				max = scores[id];
				maxId = id;
			}
		});

		return maxId;
	}
};
